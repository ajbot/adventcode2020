import re


#make a list of letters for answers
answers = []
alpha = 'a'
for i in range(0, 26): 
    answers.append(alpha) 
    alpha = chr(ord(alpha) + 1)  

#go through the groups 
with open("input.txt") as f1:
    data = f1.read()
    groups = re.split("\n\n", data)
    sum=0
    for g in groups:
        letters = []
        for l in g:
            if not l=='\n':
                letters.append(l)
        peeps = re.split("\n", g)
        for a in answers:
            if letters.count(a) == len(peeps):
                sum+=1
        
    print(sum)
