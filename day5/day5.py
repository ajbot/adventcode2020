import math
rows = list(range(1,128))
columns = list(range(1,8))
boardingIds = []

with open("input.txt") as f1:
    seats = f1.readlines()
    for code in seats:
        code = code.strip()
        mySeatRow = 0
        mySeatCol = 0        
        row = rows
        column = columns
        myRow = code[:7]
        myCol = code[7:]
        #print(code, myRow, myCol)
        for r in myRow:
            split = math.floor(len(row)/2)
            if r == "F":
                if(len(row) > 1):
                    row = list(row[:split])
            elif r == "B":
                if(len(row) > 1):
                    row = list(row[split:])
            mySeatRow = int(row[0])
        for c in myCol:
            split = math.floor(len(column)/2)
            if c == "L":
                if(len(column) > 1):
                    column = list(column[:split])
            elif c == "R":
                if(len(column) > 1):
                    column = list(column[split:])
            mySeatCol = int(column[0])
        bid = mySeatRow * 8 + mySeatCol
        boardingIds.append(bid)
    allIds = list(range(min(boardingIds),max(boardingIds)))
    ##part2
    missingseat=[]
    for i in allIds:
        if(i not in boardingIds):
            missingseat.append(i)

    print(max(boardingIds))
    print(missingseat)

