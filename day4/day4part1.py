import re 

good=0
fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

with open("input.txt") as f1:
    passports = f1.read()
    data = re.split("\n\n", passports)
    print(len(data))
    for d in data:
        parts = re.split("\ |\n", d)
        mydic={}
        for p in parts:
            key=p.split(":")[0]            
            value=p.split(":")[1]
            mydic[key]=value
        if(len(mydic) == 8):
            good+=1
        elif(len(mydic) < 7):
            if("cid" not in mydic):
                continue
        elif(len(mydic) == 7):
            if("cid" not in mydic):
                good+=1
            else:
                continue
print(good)


# byr (Birth Year)
# iyr (Issue Year)
# eyr (Expiration Year)
# hgt (Height)
# hcl (Hair Color)
# ecl (Eye Color)
# pid (Passport ID)
# cid