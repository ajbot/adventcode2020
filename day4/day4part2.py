import re 

def validateDic(myd):
    invalid=False
    ecolors = ("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
    byr = int(myd['byr'].strip())
    iyr = int(myd['iyr'].strip())
    eyr = int(myd['eyr'].strip())
    hgt = myd['hgt'].strip()
    hcl = myd['hcl'].strip()
    ecl = myd['ecl'].strip()
    pid = myd['pid'].strip()

    if('cm' in hgt):
        hgt=int(hgt.replace('cm',''))
        if(hgt < 150 or hgt > 193):
            print(hgt, "is invalid hgt in cm")
            return invalid
    elif('in' in hgt):
        hgt=int(hgt.replace('in',''))
        if(hgt < 59 or hgt > 76):
            print(hgt, "is invalid hgt in inch")
            return invalid
    else:
        return invalid

    rehcl = re.compile(r'^#[a-f,0-9]{6}$')
    repid = re.compile(r'^[0-9]{9}$')
    #print(byr, iyr, eyr, hgt, hcl, ecl, pid)
    if(byr < 1920 or byr > 2002):
        print(byr, "is invalid byr")
        return invalid
    elif(iyr < 2010 or iyr > 2020):
        print(iyr, "is invalid iyr")
        return invalid
    elif(eyr < 2020 or eyr > 2030):
        print(eyr, "is invalid eyr")
        return invalid
    elif(not rehcl.search(hcl)):
        print(hcl, "is invalid hcl")
        return invalid
    elif(len(ecl) != 3 or not ecl.endswith(ecolors)):
        print(ecl, "is invalid ecl")
        return invalid
    elif(not repid.search(pid)):
        print(pid, "is invalid pid")
        return invalid
    else:
        return True




good=0

with open("input.txt") as f1:
    passports = f1.read()
    data = re.split("\n\n", passports)
    print(len(data))
    for d in data:
        parts = re.split("\ |\n", d)
        mydic={}
        for p in parts:
            key=p.split(":")[0]            
            value=p.split(":")[1]
            mydic[key]=value
        #print("my d is ", mydic)
        if(len(mydic) == 8 and validateDic(mydic)):
            good+=1
            print(mydic)
        elif(len(mydic) == 7):
            if("cid" not in mydic and validateDic(mydic)):
                good+=1
                print(mydic)
            else:
                continue
        else:
            continue
print(good)
