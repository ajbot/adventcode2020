with open("input.txt") as f1:
    lines = set(f1.read().splitlines())
    validpwds=0
    invalidpwds=0
    for line in lines:
        count=0
        password=line.split()
        min=int(password[0].split("-")[0])
        max=int(password[0].split("-")[1])
        letter=password[1].strip(":")
        pw = password[2]
        for p in pw:
            if p == letter:
                count+=1
        if count >= min and count <= max:
            validpwds+=1
        else:
            invalidpwds+=1
        print(count, min, max, letter, pw)
        #make sure it's parsing
    
    print("valid: %s. invalid: %s" % (validpwds, invalidpwds))


