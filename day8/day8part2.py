
import re

def checkExec(instructions):
    executed=[]
    acclumator=0
    location=0
    done=False
    while location < len(instructions): 
        executed.append(location)
        if executed.count(location) > 1:
            return executed
        else:
            action=instructions[location].split(' ')[0]
            change=instructions[location].split(' ')[1]
            if('acc' in action and '-' in change):
                number=(int)(change.split('-')[1])
                acclumator-=number
                location+=1
            elif('acc' in action and '+' in change):
                number=(int)(change.split('+')[1])                
                acclumator+=number
                location+=1
            elif('jmp' in action and '-' in change):
                number=(int)(change.split('-')[1])                
                location -= number
            elif('jmp' in action and '+' in change):
                number=(int)(change.split('+')[1])                
                location += number
            else:
                location+=1
    print(acclumator)
    done="done"
    return done


with open("input.txt") as f1:    
    oginstructions=list(f1.readlines())
    executed=[]
    done=False
    count=0
    while not (done=="done"):
        instructions=oginstructions.copy()
        lastind = count
        if('jmp' in instructions[lastind] and lastind not in executed):
            instructions[lastind] = instructions[lastind].replace('jmp','nop')
        elif('nop' in instructions[lastind] and lastind not in executed):
            instructions[lastind] = instructions[lastind].replace('nop','jmp')
        done=checkExec(instructions)
        executed.append(lastind)
        count+=1
                
#parts = re.split("\ |\n", d)
# for instruction in instructions: # n items
#     while not finished or looping: # up to n iterations before finishing or looping
#         next_instruction = compute_things(instruction)