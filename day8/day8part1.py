
import re

def checkExec(instructions):
    executed=[]
    acclumator=0
    location=0
    done=False
    while location <= len(instructions): 
        executed.append(location)
        if executed.count(location) > 1:
            print(acclumator)
            return executed
        else:
            action=instructions[location].split(' ')[0]
            change=instructions[location].split(' ')[1]
            if('acc' in action and '-' in change):
                number=(int)(change.split('-')[1])
                acclumator-=number
                location+=1
            elif('acc' in action and '+' in change):
                number=(int)(change.split('+')[1])                
                acclumator+=number
                location+=1
            elif('jmp' in action and '-' in change):
                number=(int)(change.split('-')[1])                
                location -= number
            elif('jmp' in action and '+' in change):
                number=(int)(change.split('+')[1])                
                location += number
            else:
                location+=1
    print(acclumator)
    return done
    


with open("input.txt") as f1:    
    instructions=list(f1.readlines())
checkExec(instructions)

                
#parts = re.split("\ |\n", d)
# for instruction in instructions: # n items
#     while not finished or looping: # up to n iterations before finishing or looping
#         next_instruction = compute_things(instruction)