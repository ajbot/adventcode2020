with open("input.txt") as f1:
    lines = f1.readlines()
    count=0
    linesize=0
    goright=0
    tree=0
    slope = iter(lines)
    next(slope, None)
    for line in slope:
        linesize = len(line.strip())
        goright+=3
        if goright >= linesize:
            goright=goright-linesize
        if(line[goright] == "#"):
            tree+=1
print(tree)

    #289