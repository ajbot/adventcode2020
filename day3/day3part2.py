def slopeCalc(r, d):
    linesize=0
    goright=0
    tree=0
    if d==1:
        with open("input.txt") as f1:
            lines = f1.readlines()
            slope = iter(lines)
            next(slope, None)
            for line in slope:
                linesize = len(line.strip())
                goright+=r
                if goright >= linesize:
                    goright=goright-linesize
                if(line[goright] == "#"):
                    tree+=1
    else:
        with open("input.txt") as f1:
            lines = f1.readlines()
            slope = iter(lines)
            i=0
            while i < d:
                next(slope, None)
                i+=1
            for line in slope:
                next(slope, None)
                linesize = len(line.strip())
                goright+=r
                if goright >= linesize:
                    goright=goright-linesize
                if(line[goright] == "#"):
                    tree+=1      
    return(tree)


a=slopeCalc(1,1)
b=slopeCalc(3,1)
c=slopeCalc(5,1)
d=slopeCalc(7,1)
e=slopeCalc(1,2)

total=a*b*c*d*e
print(total)