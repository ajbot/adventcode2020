
with open("input.txt") as f1:
  a_lines = set(f1.read().splitlines())
  b_lines = a_lines
  c_lines = b_lines
  for linea in a_lines:
    for lineb in b_lines: 
        for linec in c_lines: 
            if int(linea) + int(lineb) + int(linec) == 2020:
                print(int(linea) * int(lineb) * int(linec))